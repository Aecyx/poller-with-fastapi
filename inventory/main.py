from fastapi import FastAPI
from pydantic import BaseModel


class Automobile(BaseModel):
    vin: str
    year: int
    make: str
    model: str
    color: str

# this is our inventory, it doesn't change much.
# No database needed :-)
inventory = [
    {
        "vin": "1234",
        "make": "Ford",
        "model": "Fiesta",
        "year": "1990",
        "color": "white",
    },
    {
        "vin": "2345",
        "make": "Yugo",
        "model": "Slo",
        "year": "1991",
        "color": "yellow",
    },
]

app = FastAPI()

@app.get("/api/inventory", response_model=list[Automobile])
def get_inventory():
    return inventory
