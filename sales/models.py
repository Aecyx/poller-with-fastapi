from pydantic import BaseModel


class AutoVO(BaseModel):
    vin: str
    year: int
    make: str
    model: str
    color: str
